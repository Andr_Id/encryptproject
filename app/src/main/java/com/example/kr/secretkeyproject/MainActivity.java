package com.example.kr.secretkeyproject;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText dataEditText;
    private EditText passwordEditText;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button encryptButton = (Button) findViewById(R.id.encryptButton);
        Button decryptButton = (Button) findViewById(R.id.decryptButton);
        this.dataEditText = (EditText) findViewById(R.id.dataEditText);
        this.passwordEditText = (EditText) findViewById(R.id.passwordEditText);
        this.textView = (TextView) findViewById(R.id.textView);

        encryptButton.setOnClickListener(this);
        decryptButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        String password = passwordEditText.getText().toString();

        switch (view.getId()) {
            case R.id.encryptButton:
                String inputString = dataEditText.getText().toString();
                try {
                    String encrypted = encrypt(inputString, password);
                    textView.setText(encrypted);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.decryptButton:
                try {
                    String decryptedString = decrypt(textView.getText().toString(), password);
                    textView.setText(decryptedString);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    private String encrypt(String data, String password) throws Exception {
        SecretKeySpec key = generateKey(password);
        Cipher c = Cipher.getInstance("AES");
        c.init(Cipher.ENCRYPT_MODE, key);
        byte[] encVal = c.doFinal(data.getBytes());
        String encryptedValue = Base64.encodeToString(encVal, Base64.DEFAULT);
        return encryptedValue;
    }

    private String decrypt(String data, String password) throws Exception {
        SecretKeySpec key = generateKey(password);
        Cipher c = Cipher.getInstance("AES");
        c.init(Cipher.DECRYPT_MODE, key);
        byte[] decodedValue = Base64.decode(data, Base64.DEFAULT);
        byte[] decValue = c.doFinal(decodedValue);
        return new String(decValue);
    }

    private SecretKeySpec generateKey(String password) throws Exception {
        final MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] bytes = password.getBytes("UTF-8");
        digest.update(bytes, 0, bytes.length);
        byte[] key = digest.digest();
        SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
        return secretKeySpec;
    }

    public static SecretKey generateKey() throws NoSuchAlgorithmException {
        // Generate a 256-bit key
        final int outputKeyLength = 256;

        SecureRandom secureRandom = new SecureRandom();
        // Do *not* seed secureRandom! Automatically seeded from system entropy.
        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(outputKeyLength, secureRandom);
        SecretKey key = keyGenerator.generateKey();
        return key;
    }
}
